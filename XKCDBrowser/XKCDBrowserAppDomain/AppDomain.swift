//
//  AppDomain.swift
//  XKCDBrowserAppDomain
//
//  Created by vikingosegundo on 20.10.23.
//

import Foundation
import XKCDBrowserModel

public typealias  Input = (Message) -> ()
public typealias Output = (Message) -> ()
public typealias AppStore = Store<AppState,AppState.Change>

public func createAppDomain(
    store      : AppStore,
    fetcher    : NetworkFetching,
    receivers  : [Input],
    rootHandler: @escaping Output
) -> Input
{
    let features: [Input] = [
        createXKCDFeature(store:store, networkFetcher:fetcher, output:rootHandler)
    ]
    
    return { msg in
        (receivers + features).forEach {
            $0(msg)
        }
    }
}
