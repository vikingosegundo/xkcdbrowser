//
//  RandomFetcher.swift
//  XKCDBrowserAppDomain
//
//  Created by vikingosegundo on 23.10.23.
//

import XKCDBrowserModel

public struct RandomFetcher:UseCase {
    public enum Request {
        case fetch(Fetch)
        public enum Fetch {
            case random
        }
    }
    public enum Response {
        case fetched(Fetched)
        public enum Fetched{
            case successfully(ComicStrip)
            case failed(NetworkfetchingError)
        }
    }
    
    public init(networkFetcher n:NetworkFetching,store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        networkFetcher = n
        store = s
        respond = r
    }
    
    func request(to request: Request) {
        switch request {
        case .fetch(.random): networkFetcher.comicStrip(with: Int.random(in: 1...store.state().maximumID)) {
            switch $0 {
            case let .success(cs): store.change(.add(.selectedComicStrip(cs))); respond(.fetched(.successfully(cs)))
            case let .failure(e) : store.change(.remove(.selectedComicStrip)) ; respond(.fetched(.failed(e)))
            }
        }
        }
    }
    private let networkFetcher:NetworkFetching
    private let store: Store<AppState,AppState.Change>
    private let respond: (Response) -> ()
    
    typealias RequestType = Request
    typealias ResponseType = Response
}
