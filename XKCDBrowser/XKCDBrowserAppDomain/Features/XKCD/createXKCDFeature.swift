//
//  createXKCDFeature.swift
//  XKCDBrowserAppDomain
//
//  Created by vikingosegundo on 20.10.23.
//

import XKCDBrowserModel

public func createXKCDFeature(
             store s: AppStore,
    networkFetcher n: NetworkFetching,
          output out: @escaping Output
) -> Input {
    
    let todayFetcher    = TodayFetcher   (networkFetcher:n, store:s, responder:process(on:out))
    let previousFetcher = PreviousFetcher(networkFetcher:n, store:s, responder:process(on:out))
    let nextFetcher     = NextFetcher    (networkFetcher:n, store:s, responder:process(on:out))
    let randomFetcher   = RandomFetcher  (networkFetcher:n, store:s, responder:process(on:out))

    func execute(cmd:Message.XKCD.Load) {
        switch cmd {
        case     .today       : todayFetcher   .request(to: .fetch)
        case     .random      : randomFetcher  .request(to: .fetch(.random))
        case let .next(cs)    : nextFetcher    .request(to: .fetch(.next(for: cs)))
        case let .previous(cs): previousFetcher.request(to: .fetch(.previous(for: cs)))
        }
    }
    
    return {
        if case let .xkcd(.load(cmd)) = $0 { execute(cmd: cmd)}
    }
}

func process(on out:@escaping Output) -> (TodayFetcher.Response) -> () {
    { 
        switch $0 {
        case let .fetched(.successfully(cs)): out(.xkcd(.loaded(.today(.successfully(cs)))))
        case     .fetched(.failed(.unknown)): out(.xkcd(.loaded(.today(.failed(NetworkfetchingError.unknown)))))
        }
    }
}
func process(on out:@escaping Output) -> (PreviousFetcher.Response) -> () {
    {
        switch $0 {
        case let .fetched(.successfully(cs)): out(.xkcd(.loaded(.previous(.successfully(cs)))))
        case .fetched(.failed(.unknown)): out(.xkcd(.loaded(.previous(.failed(NetworkfetchingError.unknown)))))
        }
    }
}
func process(on out:@escaping Output) -> (NextFetcher.Response) -> () {
    {
        switch $0 {
        case let .fetched(.successfully(cs)): out(.xkcd(.loaded(.next(.successfully(cs)))))
        case     .fetched(.failed(.unknown)): out(.xkcd(.loaded(.next(.failed(NetworkfetchingError.unknown)))))
        }
    }
}
func process(on out:@escaping Output) -> (RandomFetcher.Response) -> () {
    {
        switch $0 {
        case let .fetched(.successfully(cs)): out(.xkcd(.loaded(.random(.successfully(cs)))))
        case     .fetched(.failed(.unknown)): out(.xkcd(.loaded(.random(.failed(NetworkfetchingError.unknown)))))
        }
    }
}
