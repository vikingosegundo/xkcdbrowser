//
//  UseCase.swift
//  XKCDBrowserAppDomain
//
//  Created by vikingosegundo on 20.10.23.
//

protocol UseCase {
    associatedtype RequestType
    associatedtype ResponseType
    func request(to request:RequestType)
}
