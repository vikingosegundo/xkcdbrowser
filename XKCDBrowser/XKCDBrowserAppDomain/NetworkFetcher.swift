//
//  NetworkFetcher.swift
//  XKCDBrowserAppDomain
//
//  Created by vikingosegundo on 20.10.23.
//

import Foundation
import XKCDBrowserModel

public enum NetworkfetchingError:Error {
    case unknown
}
public protocol NetworkFetching {
    func todaysComicStrip(result: @escaping (Result<ComicStrip,NetworkfetchingError>) -> ())
    func comicStrip(with id:Int,result: @escaping (Result<ComicStrip,NetworkfetchingError>) -> ())
}

public final class NetworkFetcher:NetworkFetching {
    public init() {}
    public func todaysComicStrip(result: @escaping (Result<ComicStrip, NetworkfetchingError>) -> ()) {
        loadComicStripFromNetwork(url:URL(string: "https://xkcd.com/info.0.json")!, result: result)
    }
    
    public func comicStrip(with id: Int, result: @escaping (Result<ComicStrip, NetworkfetchingError>) -> ()) {
        loadComicStripFromNetwork(url:URL(string: "https://xkcd.com/\(id)/info.0.json")!, result: result)
    }
}
fileprivate func loadComicStripFromNetwork(url:URL, result: @escaping (Result<ComicStrip, NetworkfetchingError>) -> ()) {
    let urlSession = URLSession.shared
    let task = urlSession.dataTask(with: url) { data, response, error in
        guard let data = data else {
            result(.failure(.unknown))
            return
        }
        let cs = comicStripFrom(data: data)
        guard let cs = cs else { return }
        result(.success(cs))
    }
    task.resume()
}
fileprivate func comicStripFrom(data:Data) -> ComicStrip? {
    let dict = try? JSONSerialization.jsonObject(with: data) as? [String:Any]
    guard let dict = dict else { return nil }
    let cs = ComicStrip(dict["num"] as! Int,
                        dateFromComponents(
                            y:Int(dict["year"] as! String)!,
                            m:Int(dict["month"] as! String)!,
                            d:Int(dict["day"] as! String)!
                        ),
                        dict["title"] as! String,
                        dict["alt"] as! String,
                        URL(string:dict["img"] as! String)!)
    return cs
}

fileprivate func dateFromComponents(y:Int,m:Int,d:Int) -> Date {
    return Calendar.autoupdatingCurrent.date(from:DateComponents(year:y, month:m, day:d))!
}
