//
//  XKCDBrowserApp.swift
//  XKCDBrowser
//
//  Created by vikingosegundo on 20.10.23.
//

import SwiftUI
import XKCDBrowserUI
import XKCDBrowserModel
import XKCDBrowserAppDomain

@main
struct XKCDBrowserApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewState: viewState)
                .onAppear{
                    viewState.roothandler(.xkcd(.load(.today)))
                }
        }
    }
}

fileprivate let store:Store = createDiskStore()
fileprivate let fetcher     = NetworkFetcher()
fileprivate let roothandler = createAppDomain(store:store, fetcher:fetcher, receivers:[], rootHandler:{ roothandler($0) })
fileprivate let viewState   = ViewState(store:store, roothandler:roothandler)
