//
//  Message.swift
//  XKCDBrowserModel
//
//  Created by vikingosegundo on 20.10.23.
//

public enum Message {
    case xkcd(XKCD); public enum XKCD {
        case load(Load); public enum Load {
            case today
            case random
            case previous(ComicStrip)
            case next(ComicStrip)
        }
        
        case loaded(Loaded); public enum Loaded {
            case today(Outcome)
            case previous(Outcome)
            case next(Outcome)
            case random(Outcome)
            public enum Outcome {
                case successfully(ComicStrip)
                case failed(Error?)
            }
        }
    }
}
