//
//  AppState.swift
//  XKCDBrowserModel
//
//  Created by vikingosegundo on 20.10.23.
//
public typealias AppStore = Store<AppState,AppState.Change>

public struct AppState {
    public enum Change {
        case add(Add); public enum Add {
            case selectedComicStrip(ComicStrip)
        }
        case remove(Remove); public enum Remove {
            case selectedComicStrip
        }
    }
    
    public func alter(by changes:[Change]) -> Self { changes.reduce(self) { $0.alter(by:$1) } }

    private func alter(by c:Change) -> Self {
        switch c {
        case let .add(.selectedComicStrip(scs)): Self(scs,scs.id > maximumID ? scs.id : maximumID)
        case     .remove(.selectedComicStrip)  : Self(nil,maximumID)
        }
    }
    
    public init() {
        self.init(nil, 0)
    }
    private init(_ scs:ComicStrip?,_ max:Int) {
        selectedComicStrip = scs
        maximumID = max
    }
    
    public let selectedComicStrip:ComicStrip?
    public let maximumID:Int
}

extension AppState: Codable {}

