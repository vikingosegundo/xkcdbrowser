//
//  ComicStrip.swift
//  XKCDBrowserModel
//
//  Created by vikingosegundo on 20.10.23.
//

public struct ComicStrip {
    public let id       :Int
    public let published:Date
    public let title    :String
    public let altText  :String
    public let imageURL :URL
    
    enum Change {
        case id(Int)
        case published(Date)
        case title(String)
        case imageURL(URL)
    }
    
    public init(_ id: Int,_ published: Date,_ title: String,_ altText:String,_ imageURL: URL) {
        self.id        = id
        self.published = published
        self.title     = title
        self.altText   = altText
        self.imageURL  = imageURL
    }
    
    func alter(_ c:Change) -> Self {
        switch c {
        case let .id(i)       : return Self(i , published, title,altText, imageURL)
        case let .published(p): return Self(id, p        , title,altText, imageURL)
        case let .title(t)    : return Self(id, published, t    ,altText, imageURL)
        case let .imageURL(u) : return Self(id, published, title,altText, u       )
        }
    }
}

extension ComicStrip: Identifiable,Codable {}
