//
//  ContentView.swift
//  XKCDBrowser
//
//  Created by vikingosegundo on 20.10.23.
//

import SwiftUI

public struct ContentView: View {
    
    @ObservedObject
           private var viewState   : ViewState
    @State private var showAltText = false
    @State private var currentZoom = 0.0
    @State private var totalZoom   = 1.0
    
    public init(viewState vs:ViewState) {
        viewState = vs
    }
    public var body: some View {
        ZStack {
            VStack {
                if viewState.selectedComicStrip != nil {
                    Text(viewState.selectedComicStrip!.title)
                    HStack {
                        if viewState.selectedComicStrip!.id != 1 {
                            Button {
                                viewState.roothandler(.xkcd(.load(.previous(viewState.selectedComicStrip!))))
                            } label: {
                                Image(systemName: "chevron.left")
                            }
                        }
                        Text(viewState.selectedComicStrip!.published, style: .date)
                        Text("#\(viewState.selectedComicStrip!.id)")
                        
                        if viewState.selectedComicStrip!.id != viewState.maximumID {
                            Button {
                                viewState.roothandler(.xkcd(.load(.today)))
                            } label: {
                                Text("today")
                            }
                        }
                        Button {
                            viewState.roothandler(.xkcd(.load(.random)))
                        } label: {
                            Image(systemName: "shuffle")
                        }
                        if viewState.selectedComicStrip!.id != viewState.maximumID {
                            Button {
                                viewState.roothandler(.xkcd(.load(.next(viewState.selectedComicStrip!))))
                            } label: {
                                Image(systemName: "chevron.right")
                            }
                        }
                    }
                    AsyncImage(url: viewState.selectedComicStrip!.imageURL) { image in
                        image.resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(maxWidth: 400, maxHeight: 600)
                    } placeholder: {
                        Text("placeholder").frame(maxWidth: 400, maxHeight: 600)
                    }
                    .draggable()
                    .scaleEffect(currentZoom + totalZoom)
                    .gesture(
                        MagnifyGesture()
                            .onChanged { value in
                                currentZoom = value.magnification - 1
                            }
                            .onEnded { value in
                                totalZoom += currentZoom
                                currentZoom = 0
                            }
                    )
                    .onLongPressGesture(minimumDuration:0.4) {
                        showAltText.toggle()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            showAltText = false
                        }
                    }
                } else {
                    Text("placeholder")
                }
            }
        }.fullScreenCover(isPresented: $showAltText, content: {
            ZStack {
                Color.black.opacity(0.8)
                    .edgesIgnoringSafeArea(.all)
                Text(viewState.selectedComicStrip!.altText)
                    .foregroundStyle(.white)
                    .padding()
            }
        })
        .padding()
    }
}

//https://stackoverflow.com/a/63082240/106435
struct DraggableView: ViewModifier {
    @State var offset = CGPoint(x: 0, y: 0)
    
    func body(content: Content) -> some View {
        content
            .gesture(DragGesture(minimumDistance: 1)
                .onChanged { value in
                    self.offset.x += value.location.x - value.startLocation.x
                    self.offset.y += value.location.y - value.startLocation.y
            })
            .offset(x: offset.x, y: offset.y)
    }
}

extension View {
    func draggable() -> some View {
        return modifier(DraggableView())
    }
}
