//
//  ViewState.swift
//  XKCDBrowserUI
//
//  Created by vikingosegundo on 21.10.23.
//

import SwiftUI
import XKCDBrowserModel

public final class ViewState: ObservableObject {
    @Published public var selectedComicStrip: ComicStrip? = nil
    @Published public var maximumID         : Int = 0
    @Published public var roothandler       : (Message) -> ()

    public init(store:AppStore, roothandler: @escaping (Message) -> ()) {
        self.roothandler = roothandler
        store.updated { self.process(store.state()) }
        process(store.state())
    }

    public func process(_ appState: AppState) {
        DispatchQueue.main.async {
            self.selectedComicStrip = appState.selectedComicStrip
            self.maximumID = appState.maximumID
        }
    }
}
